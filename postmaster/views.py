import uuid

from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

@csrf_exempt
def main(request):
  """
  'request' is an HttpRequest object. Process
  it and return an HttpResponse object.
  """

  # make sure we got a post
  if request.method != 'POST':
    return HttpResponse("Expected a POST, got a %s." % request.method)

  # get data from post
  eeg_data = request.body

  # write it to a file
  filename = "eeg.%s.txt" % uuid.uuid4()
  with open(filename, 'w') as f:
    print >>f, eeg_data

  # reply
  return HttpResponse("Stored data in file: %s!" % filename);
